﻿#include <opencv2/core/core.hpp>
#include<opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <cstdio>
#include <String>
#include <vector>
#include "opencv2/objdetect/objdetect.hpp"
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>
#include<fstream>
using namespace cv;
using namespace std;

void trainingExtract(fstream& out, vector<cv::String> fn, int label) {
    // Khai báo 1 vector dạng MAT chưa thông tin từng ảnh
    // Số lượng ảnh trong folder
    size_t count = fn.size(); //number of png files in images folder

    // 1. Đọc thông tin ảnh (GRAYSCLE) và lưu vào vector images
    for (size_t i = 0; i < count; i++) {
        Mat src = imread(fn[i], IMREAD_GRAYSCALE);
        //imshow("posImg", src);
        Mat posImg;
        //resize ảnh chia hết cho block size, cell size)
        resize(src, posImg, Size(240, 240));

        // Khởi tạo các thông số tính HoG
        cv::HOGDescriptor hogDesc(posImg.size(), // kích thước cửa sổ
            cv::Size(8, 8),    // block size
            cv::Size(4, 4),    // block stride
            cv::Size(4, 4),    // cell size
            9);

        //khởi tạo biến lưu HoG
        std::vector<float> desc;

        //Tính HoG và lưu vào biến desc
        hogDesc.compute(posImg, desc);
        /*
                fprintf_s(fp, "%d\t", label);
                for (int j = 0; j < desc.size(); j++) {
                    fprintf_s(fp, "%d\t", j, ":%f\t", desc.at(j));
                }
                fprintf_s(fp, "\n");
                */
        out << label << " ";
        for (int j = 0; j < desc.size(); j++)
            out << j + 1 << ":" << desc[j] << " ";

        out << endl;
    }
}

// Hàm chính 
int main()
{

    //FILE* fp;
   // errno_t err;


   // err = fopen_s(&fp, "training.dat", "w+");

    //fstream outFile("training.dat");  
    fstream outFile;

    outFile.open("training.dat");
    //outFile.open("testing.dat");


    // Khai báo định danh folder chưa ảnh
    vector<cv::String> fnCir, fnSq, fnTri;
    glob("C:/Users/multi/Downloads/TrainingDataset/TrainingDataset/Circle/*.jpg", fnCir, false);
    glob("C:/Users/multi/Downloads/TrainingDataset/TrainingDataset/Square/*.jpg", fnSq, false);
    //glob("C:/Users/multi/Downloads/TrainingDataset/TrainingDataset/Triangle/*.jpg", fnTri, false);

     //glob("C:/Users/multi/Downloads/TrainingDataset/TestingImages/test_tron/*.jpg", fnCir, false);
     //glob("C:/Users/multi/Downloads/TrainingDataset/TestingImages/test_vuong/*.jpg", fnSq, false);
     //glob("C:/Users/multi/Downloads/TrainingDataset/TestingImages/test_tamgiac/*.jpg", fnTri, false);

    trainingExtract(outFile, fnCir, -1);
    trainingExtract(outFile, fnSq, 1);
    //trainingExtract(outFile, fnTri, 2);


    outFile.close();
    return 0;
}