#include <opencv2/core/core.hpp>
#include<opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <cstdio>
#include <String>
#include <vector>
#include "opencv2/objdetect/objdetect.hpp"
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>

#define pi 3.142857
using namespace cv;
using namespace std;
int main()
{
	Mat img = imread("test.jpg", CV_LOAD_IMAGE_COLOR);
	if (!img.data)
	{
		printf("Khong the mo / tim duoc anh\n");
		return -1;
	}

	Mat rgb[3];
	split(img, rgb);
	int R, G, B;
	for (int i = 0; i < img.rows; i++)
	{
		for (int j = 0; j < img.cols; j++)
		{
			R = rgb[2].at<uchar>(i, j);
			G = rgb[1].at<uchar>(i, j);
			B = rgb[0].at<uchar>(i, j);
		}
	}
	namedWindow("Orignal", 1);
	namedWindow("blue", 1);
	namedWindow("green", 1);
	namedWindow("red", 1);

	imshow("Orignal", img);
	imshow("blue", rgb[0]);
	imshow("green", rgb[1]);
	imshow("red", rgb[2]);

	imwrite("blue.jpg", rgb[0]);
	imwrite("green.jpg", rgb[1]);
	imwrite("red.jpg", rgb[2]);
	waitKey();
	return 0;
}