﻿#include <opencv2/core/core.hpp>
#include<opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <cstdio>
#include <String>
#include <vector>
#include "opencv2/objdetect/objdetect.hpp"
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>

using namespace cv;
using namespace std;

void trainingExtract(FILE* fp,vector<cv::String> fn, int label) {
    // Khai báo 1 vector dạng MAT chưa thông tin từng ảnh
    vector<Mat> images;
    // Số lượng ảnh trong folder
    size_t count = fn.size(); //number of png files in images folder

    // 1. Đọc thông tin ảnh (GRAYSCLE) và lưu vào vector images
    for (size_t i = 0; i < count; i++) {
        images.push_back(imread(fn[i], IMREAD_GRAYSCALE));
    }

    // Show ảnh - check



    for (int i = 0; i < images.size(); i++) {
        imshow("Triangle", images[i]);
        waitKey(100);
    }

    vector<float> meanList;
    vector<float> max;
    //fprintf(fp, "Hello \n");
    for (int n = 0; n < images.size(); n++) {
        float total = 0, mean = 0;
        float ma = 0;
        for (int i = 0; i < images[n].cols; i++) {
            for (int j = 0; j < images[n].rows; j++) {
                float pixel_value = (float)(images[n].data[i * images[n].rows + j]);
                total += pixel_value;
                if (ma < pixel_value)
                    ma = pixel_value;
            }
        }
        max.push_back(ma);
        mean = total / (images[n].cols * images[n].rows);
        meanList.push_back(mean);
    }

    vector<float> varList;
    for (int n = 0; n < images.size(); n++) {
        float totalVar = 0;
        for (int i = 0; i < images[n].cols; i++) {
            for (int j = 0; j < images[n].rows; j++) {
                float pixel_value2 = (float)(images[n].data[i * images[n].rows + j]);
                float val = (pixel_value2 - meanList.at(n)) * (pixel_value2 - meanList.at(n));
                totalVar += val;
            }
        }

        totalVar /= (images[n].cols * images[n].rows);
        varList.push_back(totalVar);
        fprintf(fp, "%d\t1:%f\t2:%f\n", label, meanList.at(n), varList.at(n));

    }
}

// Hàm chính 
int main()
{

    FILE* fp;
    errno_t err;
   

    err = fopen_s(&fp, "training.dat", "w+");
    //err = fopen_s(&fp, "testing.dat", "w+");

    // Khai báo định danh folder chưa ảnh
    vector<cv::String> fnCir,fnSq;
    glob("C:/Users/multi/Downloads/TrainingDataset/TrainingDataset/Circle/*.jpg", fnCir, false);
    glob("C:/Users/multi/Downloads/TrainingDataset/TrainingDataset/Square/*.jpg", fnSq, false);

   // glob("C:/Users/multi/Downloads/TrainingDataset/TestingImages/test_tron/*.jpg", fnCir, false);
   // glob("C:/Users/multi/Downloads/TrainingDataset/TestingImages/test_vuong/*.jpg", fnSq, false);

    trainingExtract(fp, fnCir, -1);
    trainingExtract(fp, fnSq, 1);

    fclose(fp);
    return 0;
}